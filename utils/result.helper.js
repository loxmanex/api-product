exports.ApiResult = class apiResult {
    constructor(status, data, message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }
};

exports.TableResult = class TableResult {
    constructor(total, item) {
        this.total = total;
        this.item = item;
    }
};