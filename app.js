const express = require('express');
const cors = require('cors');
var bodyParser = require('body-parser');
const admin = require('firebase-admin');

const {
    getAllProduct,
    getProduct
} = require('./services/products-service');

// define variable
const app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: false
}))

// parse application/json
app.use(bodyParser.json({
    limit: '5mb'
}))

// set up cors
app.use(cors());

let serviceAccount = require('./configs/api-testing-94da6-776de3b727f3.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});

let db = admin.firestore();


app.get('/v1/products', (req, res) => {
    getAllProduct(req,db,res)}
);

app.get('/v1/products/:product_id', (req, res) => {
    getProduct(req,db,res)}
);

app.listen(30022, () => {
    console.log('app now listening for requests on port 30022');
});