exports.HttpResponse = {
    OK: 200,
    BADREQUEST: 400,
    NOTFOUND: 404,
    UNAUTHORIZED: 401,
    ERROR: 500,
    FAIL:412,

    UNAUTHORIZED_MSG: "User Can't find in system",
    ERROR_MSG: "Something wrong",
    OK_MSG : "Success"
}