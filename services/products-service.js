const {
    ApiResult
} = require('../utils/result.helper');

const {HttpResponse} = require('../configs/config');



exports.getAllProduct = (req, db, res) => {

    let _apiResult = new ApiResult();

    let productsRef = db.collection('products');

    productsRef.where('quantity','>',0).get()
    .then((snapshot) => {
        let _data = [];
        snapshot.forEach((doc) => {
            console.log(doc.id, '=>', doc.data());
            
            let _response_data = doc.data();
            _response_data.id = doc.id;
            _data.push(_response_data);
        });

        _apiResult.status = HttpResponse.OK;
        _apiResult.data = _data;
        
        res.json(_apiResult); 
    })
    .catch((err) => {
        console.log('Error getting documents', err);
        _apiResult.status = HttpResponse.ERROR;
        _apiResult.message = HttpResponse.ERROR_MSG;
        res.json(_apiResult);
    });
}

exports.getProduct = (req, db, res) => {

    let _apiResult = new ApiResult();

    const _product_id = req.params.product_id;

    let productsRef = db.collection('products');

    productsRef.doc(_product_id).get()
    .then((doc) => {
            
        let _response_data = doc.data();
        _response_data.id = doc.id;

        _apiResult.status = HttpResponse.OK;
        _apiResult.data = _response_data;
        
        res.json(_apiResult); 
    })
    .catch((err) => {
        console.log('Error getting documents', err);
        _apiResult.status = HttpResponse.ERROR;
        _apiResult.message = HttpResponse.ERROR_MSG;
        res.json(_apiResult);
    });
}